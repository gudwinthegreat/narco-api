export interface IJwtUserPayload {
  userId?: string;
}

export interface IJwtClientPayload {
  clientId?: string;
}

export type JwtPayload = {
  realmId: string;
  roles?: string[];
} & IJwtUserPayload &
  IJwtClientPayload;

export type AuthModuleOptions = {
  jwtSecret: string;
  saltRounds: number;
  signOptions: {
    expiresIn: string;
  };
};

export interface IAuthInfo {
  id: string;
  realmId: string;
  encryptedPassword?: string;
  publicKey?: string;
  roles?: string[];
}

export interface IAuthProvider {
  getAuthInfo(findObj: any): Promise<IAuthInfo>;
  getPublicKey(id: string): Promise<string>;
}

export interface ISigninResult {
  token: string;
}

export interface AuthModuleOptionsFactory {
  createAuthOptions(): Promise<AuthModuleOptions> | AuthModuleOptions;
}
