import { Global, Inject, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import { AuthModuleOptions } from 'src/modules/auth/auth.model';


@Global()
@Injectable()
export class AppConfigService {
  constructor(
    @Inject(ConfigService) private readonly configService: ConfigService,
  ) {}

  createTypeOrmOptions(): TypeOrmModuleOptions {
    return this.configService
      .get('databases')
      .find((db) => db.name === 'default');
  }

  createAuthOptions(): AuthModuleOptions {
    return this.configService.get('auth');
  }
}
