import { AuthModuleOptions } from "src/modules/auth/auth.model";
import { TypeOrmModuleOptions } from '@nestjs/typeorm';

export default () => ({
    host: process.env.HOST || '0.0.0.0',
    port: parseInt(process.env.PORT, 10) || 3000,

    auth: {
        jwtSecret: process.env.JWT_SECRET,
        saltRounds: 8,
        signOptions: {
            expiresIn: process.env.JWT_EXPIRES_IN,
        },
    } as AuthModuleOptions,


    databases: [
        {
            name: 'default',
            type: process.env.DATABASE_TYPE || 'postgres',
            database: process.env.DATABASE_NAME || 'postgres',
            host: process.env.DATABASE_HOST || 'localhost',
            port: parseInt(process.env.DATABASE_PORT, 10) || 5432,
            username: process.env.DATABASE_USER || 'admin',
            password: process.env.DATABASE_PASSWORD || 'admin',
            autoLoadEntities: true,

            entities:["dist/**/*.entity{.ts,.js}"],
            synchronize:true,
            
            // migrations: ["dist/src/**/db/migrations/*{.ts,.js}"]
            // cli:{
            //     migrationsDir: "db/migrations"
            // }
        },
    ] as TypeOrmModuleOptions[],
})